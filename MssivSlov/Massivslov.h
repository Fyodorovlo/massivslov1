#pragma once
#include <iostream>
typedef struct Item {
public:
	char ch;
	Item * next;
	Item * down;

};

const int itemcpy(Item &dest, const Item&source);
int deletelist(Item* list);

class Massivslov {
private:
	Item * ptr;
public:
	Massivslov();
	Massivslov(const Massivslov& M);
	Massivslov(Massivslov&& M);
	Massivslov(Item *curr);
	Item * getter(Item *yy);
	void setter(Item *ff);
	int search(Item *gg);
	Item * symbol(char ch);
	void sort();
	friend std::ostream& operator <<(std::ostream&, const Massivslov &);
	friend std::istream& operator >>(std::istream&, Massivslov &);
	const Massivslov& operator =(const Massivslov &);
	Item* Massivslov ::operator [ ] (const int i) const;
	~Massivslov();
};